/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shane.mapreduce;

/**
 *
 * @author Shane
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class MapReduce {
    
    private double failureRate = 0.0;
    private Random rand = new Random(System.currentTimeMillis());
    
    public MapReduce(int failRate) {
        this.failureRate = failRate/(double)100.0;
    }

    public void map(String file, String contents, IMapCallback<String, MappedItem> callback) throws Exception {
        if(threadFail()) {
            throw new Exception("Thread failure while mapping file " + file);
        }
        String[] words = contents.trim().split("\\s+");
        List<MappedItem> results = new ArrayList<MappedItem>(words.length);
        for (String word : words) {
            results.add(new MappedItem(word, file));
        }
        callback.mapDone(file, results);
        System.err.println("Done mapping on file " + file);
    }

    public void reduce(String word, List<String> list, IReduceCallback<String, String, Integer> callback) throws Exception {
        if(threadFail()) {
            throw new Exception("Thread failure while reducing for word " + word);
        }
        Map<String, Integer> reducedList = new HashMap<String, Integer>();
        for (String file : list) {
            Integer occurrences = reducedList.get(file);
            if (occurrences == null) {
                reducedList.put(file, 1);
            } else {
                reducedList.put(file, occurrences.intValue() + 1);
            }
        }
        callback.reduceDone(word, reducedList);
        System.err.println("Done mapping on file " + word);
    }

    private synchronized boolean threadFail() {
        double random = rand.nextDouble();
        if(Double.compare(this.failureRate, random) > 0) {
            return true;
        }
        return false;
    }

    
}
