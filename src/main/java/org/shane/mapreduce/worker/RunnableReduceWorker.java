/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shane.mapreduce.worker;

import java.util.List;
import org.shane.mapreduce.IReduceCallback;
import org.shane.mapreduce.MapReduce;

/**
 *
 * @author 10340427
 */
public class RunnableReduceWorker implements Worker {

    private String word;
    private List<String> list;
    private MapReduce reducer;
    private IReduceCallback<String, String, Integer> reduceCallback;
    private boolean isFailed = false;

    public RunnableReduceWorker(String word, List<String> list, MapReduce reducer, IReduceCallback<String, String, Integer> callback) {
        this.word = word;
        this.list = list;
        this.reducer = reducer;
        this.reduceCallback = callback;
    }

    public void run() {
        try {
            getReducer().reduce(getWord(), getList(), getReduceCallback());
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            setIsFailed(true);
        }
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @return the list
     */
    public List<String> getList() {
        return list;
    }

    /**
     * @return the reducer
     */
    public MapReduce getReducer() {
        return reducer;
    }

    /**
     * @return the reduceCallback
     */
    public IReduceCallback<String, String, Integer> getReduceCallback() {
        return reduceCallback;
    }

    /**
     * @return the isFailed
     */
    public boolean isFailed() {
        return isFailed;
    }

    /**
     * @param isFailed the isFailed to set
     */
    public void setIsFailed(boolean isFailed) {
        this.isFailed = isFailed;
    }
    
    @Override
    public String toString() {
        return "Task on " + this.word;
    }
}

