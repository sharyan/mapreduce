/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shane.mapreduce.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manages the worker threads that are used for the mapping part of the
 * MapReduce Algorithm
 *
 * @author 10340427
 */
public class WorkerManager<T extends Worker> {

    private ExecutorService executor = null;
    private int maxThreads = 10;
    private List<T> workers;

    public WorkerManager(int maxThreads, List<T> workers) {
        this.maxThreads = maxThreads;
        this.workers = workers;
    }

    public void runWorkers() {
        do {
            executor = Executors.newFixedThreadPool(maxThreads);
            // Resubmission logic
            if(!isComplete()) {
                this.workers = getFailedWorkers();
            }
            
            for (T worker : this.workers) {
                executor.submit(worker);
            }
            executor.shutdown();
            try {
                executor.awaitTermination(1000, TimeUnit.SECONDS);
            } catch (InterruptedException ex) {
                Logger.getLogger(WorkerManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while(!isComplete());
    }

    public void runWorkers(List<T> runnables) {
        this.workers = runnables;
        runWorkers();
    }

    private boolean isComplete() {
        for (T worker : workers) {
            if (worker.isFailed()) {
                return false;
            }
        }
        return true;
    }

    private List<T> getFailedWorkers() {
        List<T> failed = new ArrayList<T>();
        for (T worker : workers) {
            if (worker.isFailed()) {
                failed.add(worker);
                worker.setIsFailed(false);
                System.err.println("Resubmitting task " + worker.toString());
            }
        }
        return failed;
    }
}
