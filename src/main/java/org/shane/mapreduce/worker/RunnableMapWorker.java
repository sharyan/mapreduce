/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shane.mapreduce.worker;

import org.shane.mapreduce.IMapCallback;
import org.shane.mapreduce.MapReduce;
import org.shane.mapreduce.MappedItem;

/**
 *
 * @author 10340427
 */
public class RunnableMapWorker implements Worker {

    private String file;
    private String contents;
    private MapReduce mapper;
    private IMapCallback<String, MappedItem> mapCallback;
    private boolean isFailed = false;

    public RunnableMapWorker(String file, String contents, MapReduce mapper, IMapCallback<String, MappedItem> callback) {
        this.file = file;
        this.contents = contents;
        this.mapper = mapper;
        this.mapCallback = callback;
    }

    public void run() {
        try {
            getMapper().map(getFile(), getContents(), getMapCallback());
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            setIsFailed(true);
        }
    }
    
    @Override
    public boolean isFailed() {
        return isFailed;
    }

    /**
     * @return the file
     */
    public String getFile() {
        return file;
    }

    /**
     * @return the contents
     */
    public String getContents() {
        return contents;
    }

    /**
     * @return the mapper
     */
    public MapReduce getMapper() {
        return mapper;
    }

    /**
     * @return the mapCallback
     */
    public IMapCallback<String, MappedItem> getMapCallback() {
        return mapCallback;
    }

    /**
     * @param isFailed the isFailed to set
     */
    public void setIsFailed(boolean isFailed) {
        this.isFailed = isFailed;
    }
    
    @Override
    public String toString() {
        return "Task on " + this.file;
    }
    
}