/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shane.mapreduce.worker;

/**
 *
 * @author Shane
 */
public interface Worker extends Runnable{
    
    public boolean isFailed();
    public void setIsFailed(boolean failed);
}
