/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shane.mapreduce;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Shane
 */
public class FileHandler {

    private String absolutePath;
    private List<String> filePaths;
    private Map<String, String> fileContents;
    private String contents;

    public FileHandler(String resourcePath) {
        this.filePaths = new ArrayList<String>();
        this.fileContents = new HashMap<String, String>();
        
        URL resources;
        File dir;
        
        if(resourcePath.isEmpty()) {
            dir = new File(System.getProperty("user.dir"));
        } 
        else {
            dir = new File(resourcePath);
        }
        
        for (File nextFile : dir.listFiles()) {
            String fileName = nextFile.getName();
            String extension = "";
            int i = fileName.lastIndexOf('.');
            int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));
            if (i > p) {
                extension = fileName.substring(i + 1);
            }
            if (!extension.isEmpty()) {
                if (extension.equalsIgnoreCase("txt") || extension.equalsIgnoreCase("log")) {
                    filePaths.add(nextFile.getAbsolutePath());
                }
            }
        }
    }

    public void readFiles() {
        for (String path : filePaths) {
            File textFile = new File(path);
            try {
                String content = FileUtils.readFileToString(textFile);
                this.fileContents.put(textFile.getName(), content);
            } catch (IOException ex) {
                Logger.getLogger(FileHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public Map<String, String> getFilesContents() {
        if(this.fileContents.isEmpty()) {
            System.err.println("No txt or log files where found in directory");
            System.exit(0);
        }
        
        return this.fileContents;
    }

    public int getNumberFiles() {
        return filePaths.size();
    }
}
