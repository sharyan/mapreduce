package org.shane.mapreduce;

import java.util.Map;

/**
 *
 * @author Shane
 */
public interface IReduceCallback<E, K, V>  {
    public void reduceDone(E e, Map<K, V> results);
}
