# -*- coding: utf-8 -*-
"""
Created on Thu Nov 07 13:55:59 2013

@author: Shane
"""

from nltk.text import Text
from nltk.corpus import brown
from nltk.corpus import reuters
from string import split
from random import Random


def generate_logs():
    rand = Random()
    categories=reuters.categories()
    
    for i in range(0, 10):
        catagory = categories[rand.randint(0, len(categories)-1)]
        text = Text(reuters.words(categories=catagory))
        generated = text.generate(length=10000)
        generated = generated.lower()
        tokens = split(generated, ' ')
        out = ""
        for token in tokens:
            if token.lower().isalpha() and len(token) > 3:
                out += token + " "
        log_name = 'log' + str(i+1) + '.log'
        f = open(log_name, 'w')
        f.write(out)
    
if __name__ == '__main__':
    generate_logs()
