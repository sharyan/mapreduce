package org.shane.mapreduce;

import java.util.List;

/**
 *
 * @author Shane
 */
public interface IMapCallback<E, V> {

    public void mapDone(E key, List<V> values);
}
