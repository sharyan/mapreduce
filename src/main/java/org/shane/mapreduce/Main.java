/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shane.mapreduce;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.shane.mapreduce.worker.RunnableMapWorker;
import org.shane.mapreduce.worker.RunnableReduceWorker;
import org.shane.mapreduce.worker.WorkerManager;

/**
 *
 * @author Shane
 */
public class Main implements IMapCallback<String, MappedItem>, IReduceCallback<String, String, Integer> {

    private final Map<String, Map<String, Integer>> output = new HashMap<String, Map<String, Integer>>();
    private final List<MappedItem> mappedItems = new LinkedList<MappedItem>();

    private static void usage(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("MapReduce", options);
    }

    public static void main(String[] args) {

        // create the command line parser
        CommandLineParser parser = new BasicParser();
        // create the Options
        Options options = new Options();
        options.addOption(OptionBuilder.withLongOpt("files")
                .withDescription("Directory of files to be indexed, must be fully qualified. By default searches the current running directory for txt or log files")
                .hasArg()
                .withArgName("location")
                .create());
        options.addOption(OptionBuilder.withLongOpt("error")
                .withDescription("Sets the simulated failure rate of the threads in the cluster")
                .hasArg()
                .withArgName("failrate")
                .create());
        options.addOption(OptionBuilder.withLongOpt("thread")
                .withDescription("Sets the maximum number of threads")
                .hasArg()
                .withArgName("maxThreadCount")
                .create());


        String fileLocation = "";
        int failureRate = 0;
        int maxThreads = 10;


        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);
            if (line != null) {
                if (line.hasOption("files")) {
                    fileLocation = line.getOptionValue("files");
                    System.out.println("File location: " + fileLocation);
                }
                if (line.hasOption("error")) {
                    failureRate = Integer.parseInt(line.getOptionValue("error"));
                    System.out.println("Failure rate: " + failureRate);
                }
                if (line.hasOption("thread")) {
                    maxThreads = Integer.parseInt(line.getOptionValue("thread"));
                    System.out.println("Max number of threads: " + maxThreads);
                }
            }
        } catch (ParseException exp) {
            usage(options);
            System.exit(0);
        }



        Main main = new Main();
        main.doMapReduce(fileLocation, maxThreads, failureRate);
    }

    private void doMapReduce(String resourcePath, int maxThreads, int failureRate) {

        FileHandler handler = new FileHandler(resourcePath);
        handler.readFiles();
        Map<String, String> fileContents = handler.getFilesContents();

        final MapReduce mapReduce = new MapReduce(failureRate);

        double startMappingTime = System.currentTimeMillis();
        doMapping(fileContents, mapReduce, maxThreads);
        double endMappingTime = System.currentTimeMillis();
        
        Map<String, List<String>> groupedItems = doGrouping();

        double startReduceTime = System.currentTimeMillis();
        doReduce(groupedItems, mapReduce, maxThreads);
        double endReduceTime = System.currentTimeMillis();
        
        // write out the MapReduce to a results file
        File resultsFile = new File(System.getProperty("user.dir") + "/mapreduce.txt");
        System.out.println("Writing results to " + System.getProperty("user.dir") + "\\mapreduce.txt");
        
        System.out.println("Mapping proceedure took " + (endMappingTime - startMappingTime) + " ms");
        System.out.println("Reduce proceedure took " + (endReduceTime - startReduceTime) + " ms");
        
        try {
            FileUtils.writeStringToFile(resultsFile, formatOutput(output).toString());
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private Map<String, List<String>> doGrouping() {
        Map<String, List<String>> groupedItems = new HashMap<String, List<String>>();
        Iterator<MappedItem> mappedIter = mappedItems.iterator();

        while (mappedIter.hasNext()) {
            MappedItem item = mappedIter.next();
            String word = item.getWord();
            String file = item.getFile();
            List<String> list = groupedItems.get(word);
            if (list == null) {
                list = new LinkedList<String>();
                groupedItems.put(word, list);
            }
            list.add(file);
        }
        return groupedItems;
    }

    private void doMapping(Map<String, String> files, final MapReduce mapper, int maxThreads) {
        final IMapCallback<String, MappedItem> mapCallback = this;
        List<RunnableMapWorker> workers = new ArrayList<RunnableMapWorker>();

        Iterator<Map.Entry<String, String>> inputIter = files.entrySet().iterator();
        while (inputIter.hasNext()) {
            Map.Entry<String, String> entry = inputIter.next();
            final String file = entry.getKey();
            final String contents = entry.getValue();
            workers.add(new RunnableMapWorker(file, contents, mapper, mapCallback));
        }

        WorkerManager manager = new WorkerManager(maxThreads, workers);
        manager.runWorkers();
    }

    private void doReduce(Map<String, List<String>> groupedItems, final MapReduce reducer, int maxThreads) {
        ExecutorService reduceExecutor = Executors.newCachedThreadPool();
        List<RunnableReduceWorker> workers = new ArrayList<RunnableReduceWorker>();

        Iterator<Map.Entry<String, List<String>>> groupedIter = groupedItems.entrySet().iterator();
        while (groupedIter.hasNext()) {
            Map.Entry<String, List<String>> entry = groupedIter.next();
            final String word = entry.getKey();
            final List<String> list = entry.getValue();

            workers.add(new RunnableReduceWorker(word, list, reducer, this));
        }

        WorkerManager manager = new WorkerManager(maxThreads, workers);
        manager.runWorkers();
    }

    private StringBuilder formatOutput(final Map<String, Map<String, Integer>> results) {
        StringBuilder out = new StringBuilder();
        Iterator<Map.Entry<String, Map<String, Integer>>> resultsIter = results.entrySet().iterator();
        while (resultsIter.hasNext()) {
            Map.Entry<String, Map<String, Integer>> entry = resultsIter.next();
            out.append(entry.getKey());
            Map<String, Integer> value = entry.getValue();
            Iterator<Map.Entry<String, Integer>> valueIter = value.entrySet().iterator();
            while (valueIter.hasNext()) {
                Map.Entry<String, Integer> valueEntry = valueIter.next();
                out.append("\n\t - ");
                out.append(valueEntry.getKey());
                out.append(" = ");
                out.append(valueEntry.getValue());
            }
            out.append("\n");
        }
        return out;
    }

    @Override
    public synchronized void mapDone(String file, List<MappedItem> results) {
        mappedItems.addAll(results);
    }

    @Override
    public synchronized void reduceDone(String k, Map<String, Integer> v) {
        output.put(k, v);
    }
}
